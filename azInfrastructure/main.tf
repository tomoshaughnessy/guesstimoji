terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "=3.10.0"
    }
  }
}

provider "azurerm" {
    features {}
}

locals {
  abbrev {
    azurerm_resource_group = "rg"
    azurerm_cosmosdb_account = "cosmon"
    azurerm_cosmosdb_mongo_database = "cosmos"
  }
  app = "guesstimoji"
  collNames = ["boards", "emojis", "games"]
}

resource "azurerm_resource_group" "guesstimojiRg" {
  location = var.location
  name = "az-${var.org}-${local.app}-${var.env}-${local.abbrev.azurerm_resource_group}-${var.location}-01"
} 

resource "azurerm_cosmosdb_account" "dbAcct" {
  name = "az-${var.org}-${local.app}-${var.env}-${local.abbrev.azurerm_cosmosdb_account}-${var.location}-01"
  location = azurerm_resource_group.guesstimojiRg.location
  resource_group_name = azurerm_resource_group.guesstimojiRg.name
  kind = "MongoDB"
  mongo_server_version = 4.2
  offer_type = "Standard"
  network_acl_bypass_ids = []
  tags = {}

  consistency_policy {
    consistency_level = "Session"
  }

  capabilities {
    name = "EnableServerless"
  }

  capabilities {
    name = "EnableMongo"
  }

  geo_location {
    location = azurerm_resource_group.guesstimojiRg.location
    failover_priority = 0
  }
}

resource "azurerm_cosmosdb_mongo_database" "db" {
  name = local.app
  resource_group_name = azurerm_resource_group.guesstimojiRg.name
  account_name = azurerm_cosmosdb_account.dbAcct.name
}

resource "azurerm_cosmosdb_mongo_collection" "colls" {
  for_each = toset(local.collNames)
  name = each.key
  resource_group_name = azurerm_resource_group.guesstimojiRg.name
  account_name = azurerm_cosmosdb_account.dbAcct.name
  database_name = azurerm_cosmosdb_mongo_database.db.name
  shard_key = "_id"

  index {
      keys = ["_id"]
      unique = true
  }
  index {
      keys = ["$**"]
  }
}