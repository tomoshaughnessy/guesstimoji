variable "location" {
  type    = string
  default = "eastus"
}

variable "org" {
  type = string
  default = "2s"
  description = "The organization who owns the app"
}

variable "env" {
  type = string
  default = "dev"
  description = "The environment of the application"
}

